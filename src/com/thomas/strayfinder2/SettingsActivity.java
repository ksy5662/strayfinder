package com.thomas.strayfinder2;

import java.io.IOException;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;

import com.thomas.strayfinder2.R;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;

public class SettingsActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getFragmentManager().beginTransaction()
				.replace(android.R.id.content, new SettingsFragment()).commit();
	}
}

class SettingsFragment extends PreferenceFragment implements
		OnSharedPreferenceChangeListener {

	class QueryParser extends AsyncTask<Uri, Void, JSONArray> {
		@Override
		protected JSONArray doInBackground(Uri... uris) {
			Uri uri = uris[0];
			Connection.Response res;
			try {
				res = Jsoup.connect(uri.toString()).method(Method.GET)
						.execute();

				Assert.assertTrue(res.statusMessage(), res.statusCode() == 200);

				Log.e("res",
						"uri: " + uri.toString() + ", response: " + res.body());

				JSONObject obj = new JSONObject(res.body());
				JSONArray rows = obj.getJSONArray("rows");

				return rows;
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}

	private ListPreference s_upr_cd;
	private ListPreference s_org_cd;
	private ListPreference s_shelter_cd;

	private ListPreference s_up_kind_cd;
	private ListPreference s_kind_cd;

	private void fetchKindCd(String val) {
		if (val == null)
			val = "";
		String[] up_kind_cd = val.split(",");
		Uri uri = Uri.parse("http://www.animal.go.kr/query.do").buildUpon()
				.appendQueryParameter("pid", "animal_loss")
				.appendQueryParameter("cmd", "getJsonDataList")
				.appendQueryParameter("sqlid", "kind_saver_list").build();
		uri = uri
				.buildUpon()
				.appendQueryParameter("up_kind_cd",
						up_kind_cd.length > 1 ? up_kind_cd[1] : "").build();

		new QueryParser() {
			@Override
			protected void onPostExecute(JSONArray rows) {
				super.onPostExecute(rows);
				try {
					String[] names = new String[rows.length()];
					String[] values = new String[rows.length()];
					for (int i = 0; i < rows.length(); i++) {
						JSONObject row = rows.getJSONObject(i);
						names[i] = row.getString("k_nm");
						values[i] = names[i] + "," + row.getString("kind_cd");
					}
					s_kind_cd.setEntries(names);
					s_kind_cd.setEntryValues(values);
					s_kind_cd.setEnabled(true);
				} catch (JSONException e) {
					e.printStackTrace();
					return;
				} catch (NullPointerException e) {
					e.printStackTrace();
					return;
				}
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				s_kind_cd.setEnabled(false);
			}
		}.execute(uri);
	}

	private void fetchOrgCd(String val) {
		if (val == null)
			val = "";
		String[] upr_cd = val.split(",");
		Uri uri = Uri.parse("http://www.animal.go.kr/query.do").buildUpon()
				.appendQueryParameter("pid", "animal_loss")
				.appendQueryParameter("cmd", "getJsonDataList")
				.appendQueryParameter("sqlid", "list_statistics_set").build();
		uri = uri
				.buildUpon()
				.appendQueryParameter("s_upr_cd",
						upr_cd.length > 1 ? upr_cd[1] : "").build();

		new QueryParser() {
			@Override
			protected void onPostExecute(JSONArray rows) {
				super.onPostExecute(rows);
				try {
					String[] names = new String[rows.length()];
					String[] values = new String[rows.length()];
					for (int i = 0; i < rows.length(); i++) {
						JSONObject row = rows.getJSONObject(i);
						names[i] = row.getString("orgdown_nm");
						values[i] = names[i] + "," + row.getString("org_cd");
					}

					s_org_cd.setEntries(names);
					s_org_cd.setEntryValues(values);
					s_org_cd.setEnabled(true);
				} catch (JSONException e) {
					e.printStackTrace();
					return;
				} catch (NullPointerException e) {
					e.printStackTrace();
					return;
				}
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				s_org_cd.setEnabled(false);
			}
		}.execute(uri);
	}

	private void fetchShelterCd(String val) {
		String[] upr_cd = PreferenceManager
				.getDefaultSharedPreferences(
						SettingsFragment.this.getActivity())
				.getString("s_upr_cd", "����,").split(",");
		if (val == null)
			val = "";
		String[] org_cd = val.split(",");
		Uri uri = Uri.parse("http://www.animal.go.kr/query.do").buildUpon()
				.appendQueryParameter("pid", "animal_loss")
				.appendQueryParameter("cmd", "getJsonDataList")
				.appendQueryParameter("sqlid", "list_shelter_set").build();

		uri = uri
				.buildUpon()
				.appendQueryParameter("s_upr_cd",
						upr_cd.length > 1 ? upr_cd[1] : "")
				.appendQueryParameter("s_org_cd",
						org_cd.length > 1 ? org_cd[1] : "").build();

		new QueryParser() {
			@Override
			protected void onPostExecute(JSONArray rows) {
				super.onPostExecute(rows);
				try {
					String[] names = new String[rows.length()];
					String[] values = new String[rows.length()];
					for (int i = 0; i < rows.length(); i++) {
						JSONObject row = rows.getJSONObject(i);
						names[i] = row.getString("care_nm");
						values[i] = names[i] + ","
								+ row.getString("care_reg_no");
					}
					s_shelter_cd.setEntries(names);
					s_shelter_cd.setEntryValues(values);
					s_shelter_cd.setEnabled(true);
				} catch (JSONException e) {
					e.printStackTrace();
					return;
				} catch (NullPointerException e) {
					e.printStackTrace();
					return;
				}
			}

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				s_shelter_cd.setEnabled(false);
			}
		}.execute(uri);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences);

		s_upr_cd = (ListPreference) findPreference("s_upr_cd");
		s_org_cd = (ListPreference) findPreference("s_org_cd");
		s_shelter_cd = (ListPreference) findPreference("s_shelter_cd");

		s_up_kind_cd = (ListPreference) findPreference("s_up_kind_cd");
		s_kind_cd = (ListPreference) findPreference("s_kind_cd");

		final String[] lists = { "s_upr_cd", "s_org_cd", "s_shelter_cd",
				"s_up_kind_cd", "s_kind_cd" };
		for (String str : lists)
			updateSummary(str);

		PreferenceManager.getDefaultSharedPreferences(this.getActivity())
				.registerOnSharedPreferenceChangeListener(this);

		// Fetch options which are available based on current configuration
		fetchOrgCd(s_upr_cd.getValue());
		fetchShelterCd(s_org_cd.getValue());
		fetchKindCd(s_up_kind_cd.getValue());
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
			String key) {
		if (key.equals(s_upr_cd.getKey())) {
			fetchOrgCd(s_upr_cd.getValue());

			// Reset lower category
			s_org_cd.setValueIndex(0);
		} else if (key.equals(s_org_cd.getKey())) {
			fetchShelterCd(s_org_cd.getValue());

			// Reset lower category
			s_shelter_cd.setValueIndex(0);
		} else if (key.equals(s_up_kind_cd.getKey())) {
			fetchKindCd(s_up_kind_cd.getValue());

			// Reset lower category
			s_kind_cd.setValueIndex(0);
		}

		updateSummary(key);
	}

	private void updateSummary(String key) {
		Preference pref = findPreference(key);
		if (pref instanceof ListPreference) {
			pref.setSummary(PreferenceManager
					.getDefaultSharedPreferences(this.getActivity())
					.getString(key, "").split(",")[0]);
		}
	}
}
